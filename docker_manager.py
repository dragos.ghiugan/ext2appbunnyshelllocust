from flask import Flask, render_template, request
import logging
import sys
import os
import shlex, subprocess
from flask import json, jsonify
import json

pwdir = os.getcwd()
logging.info("current working directory is %s" % pwdir)

def restart_docker_containers(dockerComposeFile, slaveNumber=1):
    logging.info("Docker compose file path is: " + pwdir + "/" + dockerComposeFile)

    docker_compose_process = subprocess.Popen(
        ["bin/restart_docker.sh", pwdir + "/" + dockerComposeFile, str(slaveNumber)],
        # shell=True,
        # stdin=None,
        # stdout=None,
        # stderr=None,
        # close_fds=True
    )
    output = docker_compose_process.communicate()[0]
    print(output)