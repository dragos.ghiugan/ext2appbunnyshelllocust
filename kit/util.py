import csv
import json
import os
import random
import re
import time
import hashlib
from kit.log import log


def dump_response(response, name=None):
    if not os.path.isdir('dumps'):
        os.mkdir('dumps')

    content = response.text
    if not name:
        name = hashlib.md5(content.encode()).hexdigest()

    path = 'dumps/%s.html' % name
    with open(path, 'w', encoding="utf8") as f:
        f.write(content)

    log.debug("Response dump saved: %s" % path)