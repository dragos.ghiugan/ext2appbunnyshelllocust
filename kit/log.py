import logging

log = logging.getLogger("logger")
log.setLevel(logging.DEBUG)

_ch = logging.StreamHandler()
_ch.setLevel(logging.DEBUG)
log.addHandler(_ch)
