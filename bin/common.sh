#!/usr/bin/env bash

function kill_remove_docker_containers() {
    echo "Killing/stopping containers..."
    nr_containers=$(docker ps -q | wc -l | sed -e 's/^[[:space:]]*//')
    echo "Number of containers found: $nr_containers"

    if [[ "$nr_containers" -eq "0" ]]; then
       echo "No containers found to kill"
    else
        echo "Detected containers to kill"
        docker kill $(docker ps -q)
        docker rm $(docker ps -a -q)
    fi
}

function start_docker_containers() {
    echo "Starting containers..."
    docker-compose -f $docker_compose_file up --build --scale slave=$slave_number
}