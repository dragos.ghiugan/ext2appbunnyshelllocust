import re
from cssselect import GenericTranslator, SelectorError
from lxml.html import fromstring
import requests
from random import randrange
import random


map = {
  "variables": [
      {
          "extractors": [
              {
                  "name": "form_action_url",
                  "extract_type": "dom_attribute_value",
                  "attribute_name": "action",
                  "cssselector": "form#product_addtocart_form"
              },
              {
                  "name": "product_id",
                  "extract_type": "dom_attribute_value",
                  "attribute_name": "value",
                  "cssselector": "form#product_addtocart_form  input[name='product']"
              },
              {
                  "name": "form_key",
                  "extract_type": "dom_attribute_value",
                  "attribute_name": "value",
                  "cssselector": "form#product_addtocart_form  input[name='form_key']"
              },
              {
                  "name": "product_item",
                  "extract_type": "dom_attribute_value",
                  "attribute_name": "value",
                  "cssselector": "form#product_addtocart_form  input[name='item']"
              }
          ]
      },
  ],
  # "form_data": {
  #     "member_id": "JJ-TEST-ATTENDEE",
  #     "_token": "::_csrf_token",
  # }
}

def read_dom_attribute_with_css_selector(css_selector, attribute_name, document):
    try:
        expression = GenericTranslator().css_to_xpath(css_selector)
        print("Using expression: %s" % expression)

        for e in document.xpath(expression):
            return e.get(attribute_name)

    except SelectorError:
        print('Invalid selector. %s' % css_selector)
        return None


def extract_variables(variables, response):
    extracted_variables = {}

    for variable in variables:
        product_html_decoded = response.content.decode("UTF-8")
        document = fromstring(product_html_decoded)

        for var in variable['extractors']:
            var_name = var['name']
            extract_type = var['extract_type']

            if extract_type == 'dom_attribute_value':
                cssselector = var['cssselector']
                attribute_name = var['attribute_name']

                variable_value = read_dom_attribute_with_css_selector(cssselector, attribute_name, document)
                print("Computed extractor %s with value %s" % (var_name, variable_value))

                extracted_variables[var_name] = variable_value
            else:
                raise ValueError("The extract type is not recognized")

    return extracted_variables


def build_form_type(form_type, external_variables):
    form_variables = {}
    for field_name, field_value in form_type.items():
        if field_value.startswith("::"):
            external_field_name = field_value[2:]
            if external_field_name in external_variables:
                print("External Field name is %s has value %s" % (external_field_name, external_variables[external_field_name]))
                form_variables[external_field_name] = external_variables[external_field_name]
            else:
                raise ValueError("The variable %s is not found in external variables" % external_field_name)
        else:
            print("Filed name is %s with value %s" % (field_name, field_value))
            form_variables[field_name] = field_value

    return form_variables


def print_variables(variables):
    for name, value in variables.items():
        print("        - %s => %s" % (name, value))


def identify_external_variables(string, external_variables_name="external_variables"):
    is_open_mark = False
    prev_char = None
    buffer = ""
    copy_string = string
    for char in string:
        if not is_open_mark and char == "{" and prev_char == "{":
            is_open_mark = True
            continue
        elif is_open_mark and char == "}" and prev_char =="}":
            print("detected variable " + buffer)
            is_open_mark = False
            prev_char = None
            copy_string = copy_string.replace("{{" +  buffer + "}}", "\" + external_variables['" + buffer + "'] + \"")
            buffer = ""

            continue
        elif is_open_mark and char != "}":
            if is_open_mark:
                buffer = buffer + char
        prev_char = char

    return copy_string
#
# response = requests.get("https://www.xpertbeauty.ro/top-coat-andreia-hybrid-gel-fusion-shine-10-5ml")
# if response.status_code != 200:
#     raise ValueError("Status code is !== 200")
#
# external_variables = extract_variables(variables=map['variables'], response=response)
# print_variables(external_variables)

# form_data = build_form_type(map['form_data'], external_variables)
# print(str(form_data))

print(identify_external_variables("{{website}}/{{pantof}}/a"))