# coding=utf-8
from subprocess import Popen, PIPE, STDOUT
import os
import logging
import sys
from influxdb import InfluxDBClient
import requests
import yaml
from datetime import datetime
from threading import Thread
from time import sleep
import signal
import json
import mysql.connector
from mysql.connector import Error
from requests.auth import HTTPBasicAuth
import pprint
from datetime import timedelta
from pytz import timezone
from src.variables_crapper import identify_external_variables, extract_variables, in_place_identify_external_variables
import random
from urllib.parse import urlparse
from jsonschema import validate
import glob
import ssl
import socket
from requests.auth import HTTPBasicAuth
import threading


class FileService:

    @staticmethod
    def read_yml_or_dict(config_file):
        """
        Reads yaml/json file and returns dict or none if any errors

        :param config_file:
        :return:
        """
        if not os.path.exists(config_file):
            logging.info("Cannot locate configuration file. File %s does not exist!" % config_file)
            return None

        if not os.path.isfile(config_file):
            raise ValueError("%s is not a file" % config_file)

        return yaml.safe_load(open(config_file))