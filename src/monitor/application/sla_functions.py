# coding=utf-8
from subprocess import Popen, PIPE, STDOUT
import os
import logging
import sys
from influxdb import InfluxDBClient
import requests
import yaml
from datetime import datetime
from threading import Thread
from time import sleep
import signal
import json
import mysql.connector
from mysql.connector import Error
from requests.auth import HTTPBasicAuth
import pprint
from datetime import timedelta
from pytz import timezone
from src.variables_crapper import identify_external_variables, extract_variables, in_place_identify_external_variables
import random
from urllib.parse import urlparse
from jsonschema import validate
import glob
import ssl
import socket
from requests.auth import HTTPBasicAuth
import threading

def verify_mandatory_key_exists(mandatory_keys, collection):
    for mandatory_key in mandatory_keys:
        if mandatory_key not in collection.keys():
            raise ValueError("mandatory key missing: " + mandatory_key)


def evaluate_time_parameter(time_var_def):
    """
    Evaluates time parameters and return a value based on time conditions
    """
    evaluation = True
    logging.info("Current hour is " + str(datetime.now().hour))
    if "and" in time_var_def.keys():
        # we assume that all conditions are true an stop when some condition si false
        evaluation = True
        for time_condition, compare_to in time_var_def['and'].items():
            if "hour" in time_condition and " > " in time_condition:
                if datetime.now().hour < compare_to:
                    evaluation = False
                    break
            elif "hour" in time_condition and " <= " in time_condition:
                if datetime.now().hour > compare_to:
                    evaluation = False
                    break
            else:
                raise ValueError("Time condition is not recognized")
    elif "or" in time_var_def.keys():
        evaluation = False
        for time_condition, compare_to in time_var_def['or'].items():
            if "hour" in time_condition and " > " in time_condition:
                if datetime.now().hour > compare_to:
                    evaluation = True
                    break
            elif "hour" in time_condition and " <= " in time_condition:
                if datetime.now().hour <= compare_to:
                    evaluation = True
                    break
            else:
                raise ValueError("Time condition is not recognized")
    elif "type" in time_var_def.keys() and time_var_def["type"] == "timestamp_now":
        time = datetime.now().replace(tzinfo=timezone(time_var_def['timezone']))
        return int("{:%s}".format(time))
    elif "type" in time_var_def.keys() and time_var_def["type"] == "timestamp_x_hours_ago":
        time = (datetime.now() - timedelta(hours=time_var_def['hours'])).replace(tzinfo=timezone(time_var_def['timezone']))
        return int("{:%s}".format(time))
    else:
        raise ValueError("Time condition type is not recognized")

    if evaluation:
        return time_var_def['true']

    return time_var_def['false']


#@deprecated @see context service
def replace_object_parameters(object, external_variables):
    """

    Replace in dict the values set as parameters by going in depth

    :param object:
    :param external_variables:
    :return:
    """
    if not isinstance(object, dict) and not isinstance(object, list):
        return object

    if isinstance(object, dict):
        new_object = {}
        for key, value in object.items():
            if isinstance(value, dict):
                new_object[key] = replace_object_parameters(value, external_variables)
            elif isinstance(value, list):
                for k in value:
                    new_object[key] = replace_object_parameters(value, external_variables)
            elif isinstance(value, str):
                new_object[key] = in_place_identify_external_variables(value, external_variables)
            else:
                new_object[key] = value

        return new_object

    if isinstance(object, list):
        new_list = []
        for value in object:
            new_list.append(replace_object_parameters(value, external_variables))

        return new_list


#This was modified from https://serverlesscode.com/post/ssl-expiration-alerts-with-lambda/
def ssl_expiry_datetime(hostname):
    ssl_date_fmt = r'%b %d %H:%M:%S %Y %Z'

    hostname_value = None
    port = 443
    if ":" in hostname:
        hostname_parts = hostname.split(":")
        hostname_value = hostname_parts[0]
        port = int(hostname_parts[1])
    else:
        hostname_value = hostname
    logging.info("GET ssl expiration date for hostname " + hostname_value + " on port " + str(port))

    context = ssl.create_default_context()
    conn = context.wrap_socket(
        socket.socket(socket.AF_INET),
        server_hostname=hostname_value,
    )
    # 3 second timeout because Lambda has runtime limitations
    conn.settimeout(15.0)
    conn.connect((hostname_value, port))
    ssl_info = conn.getpeercert()
    # parse the string from the certificate into a Python datetime object
    return datetime.strptime(ssl_info['notAfter'], ssl_date_fmt)


def read_config(config_file):
    if not os.path.exists(config_file):
        logging.info("Cannot locate configuration file. File %s does not exist!" % config_file)
        return None

    if not os.path.isfile(config_file):
        logging.info("%s is not a file" % config_file)
        return None

    try:
        config = yaml.safe_load(open(config_file))

        return config
    except Exception as e:
        logging.info('An error while reading config file : ' + str(e))
        return None

thread_local = threading.local()

def make_request(action_specs, external_variables):
    verify = False
    if action_specs['verify'] == 1:
        verify = True

    url_fetch = in_place_identify_external_variables(
        action_specs['url'],
        external_variables
    )

    if url_fetch.startswith("/"):
        if "enforce_schema_domain" not in action_specs:
            raise ValueError("Can not fech from url without schema or domeain, set enforce_schema_domain like https://cloud.bunnyshell.com without trailing slash ")

        url_fetch = (action_specs['enforce_schema_domain'] + url_fetch)

    domain = urlparse(url_fetch).netloc
    schema = urlparse(url_fetch).scheme
    auth = None
    response = None

    if "auth" in action_specs.keys():
        auth_username = in_place_identify_external_variables(action_specs['auth']['username'], external_variables)
        auth_password = in_place_identify_external_variables(action_specs['auth']['password'], external_variables)
        auth = HTTPBasicAuth(auth_username, auth_password)

    if action_specs['type'].lower() == "get":
        headers = {}
        payload = {}
        files = []
        logging.info(" -> GET domain " + domain + ", schema " + schema + ", url: " + url_fetch)

        return requests.get(
            url_fetch,
            auth=auth,
            verify=verify,
            headers=headers,
            timeout=action_specs['timeout'],
            allow_redirects=True
        )

    elif action_specs['type'].lower() == "post":
        files = []
        payload=None
        logging.info(" -> POST domain " + domain + ", schema " + schema + ", url: " + url_fetch)

        if "form_data" in action_specs.keys():
            payload_items=[]
            for form_item_name, form_item_value in action_specs['form_data'].items():
                payload_items.append(form_item_name + "=" + str(in_place_identify_external_variables(str(form_item_value), external_variables)))
            payload = "&".join(payload_items)
        if "form_raw" in action_specs.keys():
            payload = json.dumps(action_specs['form_raw'])
        headers = action_specs['headers']

        response = requests.post(
            url_fetch,
            auth=auth,
            verify=verify,
            headers=headers,
            timeout=action_specs['timeout'],
            allow_redirects=True,
            data=payload
        )
        return response
    else:
        raise ValueError("Request type is not recognized")
