#/usr/bin/env bash
set -e

influx -execute 'SELECT SUM("value") FROM "netdata.varnish.current_poll_hit_rate.hit" WHERE ("host" =~ /^varnish-prod7e16b2_10\.110\.0\.18_864a6fedf73fc1ba17d4436288776d92.*$/) AND time >= now() - 2m fill(0)'