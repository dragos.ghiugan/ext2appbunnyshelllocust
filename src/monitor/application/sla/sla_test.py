# coding=utf-8
from subprocess import Popen, PIPE, STDOUT
import os
import logging
import sys
from influxdb import InfluxDBClient
import requests
import yaml
from datetime import datetime
from threading import Thread
from time import sleep
import signal
import json
import mysql.connector
from mysql.connector import Error
from requests.auth import HTTPBasicAuth
import pprint
import datetime
from datetime import timedelta
from pytz import timezone
from src.variables_crapper import identify_external_variables, extract_variables, in_place_identify_external_variables
import random
from urllib.parse import urlparse
from jsonschema import validate
import glob
import ssl
import socket
from sla_functions import verify_mandatory_key_exists, evaluate_time_parameter, replace_object_parameters, ssl_expiry_datetime, read_config
from sla_request import sla_request
from sla_mysql import sla_mysql
from sla_elasticsearch_query import sla_elasticsearch_query
from sla_ssl_expiration import sla_ssl_expiration
from threading import Lock
from sla_outputs import SLAFileOutput, SLAInfluxOutput, SLAEmailOutput
from sla_rabbitmq_api_queue import sla_rabbitmq_api_queue
import traceback
import uuid
from ssh_sla import sla_varnish_influx_netdata_failed_backend_connections, ssh_exec_file_remotly, sla_ssh_remote_swap, sla_ssh_remote_bunnyshell_agent, sla_ssh_remote_digital_ocean_agent, sla_ssh_uptime_more_then_x_days, sla_ssh_log_rotation_enabled
from Crypto.PublicKey import RSA
import rsa


stop_program = False
mutex_output = Lock()


def receive_signal(signal_number, frame):
    """
    Receive signal of stop program, used to stop all tests
    :param signal_number:
    :param frame:
    :return:
    """
    logging.info('Received: %d' % signal_number)
    global stop_program
    stop_program = True


def main():
    """
    Main functino which boots application and run tests
    :return:
    """
    if len(sys.argv) < 2:
        logging.info("Usage %s <config file>" % sys.argv[0])
        return None

    configDir = sys.argv[1]

    preffered_test = None
    if len(sys.argv) >= 3:
        preffered_test = sys.argv[2];

    if not os.path.isdir(configDir):
        raise ValueError("Config dir not defined")

    config_file = configDir + "/config.yml"
    parameters_file = configDir + "/parameters.yml"
    if not os.path.isfile(config_file):
        raise ValueError("Config file not found at " + config_file)

    config_data = read_config(config_file)
    sla_test_map = {
        'sla_tests': {}
    }
    sla_test_map.update(config_data)

    config_logging(sla_test_map['logging_file_path'])

    if not "parameters" in sla_test_map.keys():
        sla_test_map['parameters'] = {}

    if os.path.isfile(parameters_file):
        parameters_data = read_config(parameters_file)
        sla_test_map['parameters'].update(parameters_data)

    if "dbal_conections" in sla_test_map.keys():
        sla_test_map['dbal_conections'] = replace_object_parameters(sla_test_map['dbal_conections'], sla_test_map['parameters'])
    else:
        sla_test_map['dbal_conections'] = {}

    if "outputs" in sla_test_map:
        sla_test_map['outputs'] = replace_object_parameters(sla_test_map['outputs'], sla_test_map['parameters'])
    else:
        sla_test_map['outputs'] = {}

    for file_name in os.listdir(configDir):
        if file_name.startswith("sla_"):
            spec_data = read_config(configDir + "/" + file_name)

            logging.info(" -> Loading test " + str(file_name))
            if "test_name" in spec_data.keys():
                test_name = spec_data['test_name']
            else:
                test_name = ""

            test_name_by_file = os.path.splitext(file_name)[0]
            if len(test_name) == 0 or test_name == "":
                test_name = test_name_by_file
            logging.info("Test name computed is  " + str(test_name))

            if spec_data['is_enabled'] and (preffered_test is None or preffered_test == file_name):
                spec_data['test_name'] = test_name
                sla_test_map['sla_tests'][test_name] = spec_data
            else:
                logging.info(test_name + " - test not enabled")

    if sla_test_map is None:
        sys.exit(1)

    output_clients = factory_outputs(sla_test_map)
    # create_grafana_dashbaord(sla_test_map)
    start_tests(sla_test_map, output_clients)

def create_grafana_dashbaord(sla_test_map):
    pannels = []
    panel_json_file = './templates/grafana/sla_panel_template.json'
    panel_dashboard_model_file = './templates/grafana/dashboard_model.json'

    panel_dashboard = read_config(panel_dashboard_model_file)

    for test_name, test_config in sla_test_map['sla_tests'].items():
        panel_json = read_config(panel_json_file)
        panel_name = capitalize_each_word(" ".join(test_name.split("_")))
        panel_json['alert']['name'] = panel_name
        panel_json['title'] = panel_name
        panel_json['targets'][0]['measurement'] = 'bestvalue_' + test_name
        pannels.append(panel_json)

    panel_dashboard['panels'] = pannels

    with open('./sla_grafana_dashboard.json', 'w') as fp:
        json.dump(panel_dashboard, fp,  indent=4)

def capitalize_each_word(original_str):
    result = ""
    # Split the string and get all words in a list
    list_of_words = original_str.split()
    # Iterate over all elements in list
    for elem in list_of_words:
        # capitalize first letter of each word and add to a string
        if len(result) > 0:
            result = result + " " + elem.strip().capitalize()
        else:
            result = elem.capitalize()
    # If result is still empty then return original string else returned capitalized.
    if not result:
        return original_str
    else:
        return result

def config_logging(log_file):
    try:
        logging.basicConfig(
            filename=log_file,
            level=logging.INFO,
            format='[%(asctime)s][%(levelname)s][:%(name)s] %(message)s'
        )

    except Exception as e:
        logging.error("Cannot initialize logging in %s: %s" % (log_file, e))
        sys.exit(1)

def factory_outputs(sla_test_map):
    """
    Factory all tests outputs like influx/files
    :param sla_test_map:
    :return:
    """
    output_clients = {}
    for output_name, output_config in sla_test_map['outputs'].items():
        logging.info("[process output client] Processing output " + output_name)
        if output_config['type'] == "influx":
            logging.info("[process output client] type is influx")
            mandatory_keys = ["influx_host", "influx_port", "influx_database", "measurement_prefix", "influx_user", "influx_password"]
            verify_mandatory_key_exists(mandatory_keys, output_config)

            if output_config['influx_user'] != "" \
                    and output_config['influx_password'] != "" \
                    and output_config['influx_user'] is not None \
                    and output_config['influx_password'] is not None:
                client = InfluxDBClient(
                    host=output_config['influx_host'],
                    port=output_config['influx_port'],
                    username=output_config['influx_user'],
                    password=output_config['influx_password'],
                    verify_ssl=False,
                    timeout=5
                )
            else:
                client = InfluxDBClient(
                    host=output_config['influx_host'],
                    port=output_config['influx_port'],
                    verify_ssl=False,
                    timeout=5
                )
            client.switch_database(output_config['influx_database'])

            output_clients[output_name] = SLAInfluxOutput(client,output_config['measurement_prefix'])
            logging.info("[process output client]  influxb client created")
        elif output_config['type'] == "file":
            mandatory_keys = ["path"]
            verify_mandatory_key_exists(mandatory_keys, output_config)
            output_clients[output_name] = SLAFileOutput(output_config['path'])
        elif output_config['type'] == "email":
            mandatory_keys = ["smtp_host", "smtp_port", "smtp_user", "smtp_password", "from", "to"]
            verify_mandatory_key_exists(mandatory_keys, output_config)
            output_clients[output_name] = SLAEmailOutput(
                output_config['smtp_host'],
                output_config['smtp_port'],
                output_config['smtp_user'],
                output_config['smtp_password'],
                output_config['from'],
                output_config['to']
            )
        else:
            raise ValueError("Output type not found")

    return output_clients


def start_tests(sla_test_map, output_clients):
    """
    Starts tests in async mode or sync

    :param sla_test_map:
    :param output_clients:
    :return:
    """
    threads = []
    run_mode = sla_test_map['run_mode']
    interval = 0
    if run_mode == "async_interval":
        interval = int(sla_test_map['interval'])
    if run_mode == "async_interval" and interval <= 0:
        raise ValueError("Run mode async_interval requires that interval values should be grater then zero")

    sla_results = []
    index = 1
    for test_name, test_config in sla_test_map['sla_tests'].items():
        if run_mode == "async_interval":
            test_thread = start_test_thread(test_name, test_config, output_clients, sla_test_map)
            threads.append(test_thread)
        else:
            index = index + 1
            result, logs = run_sync_test(test_name, test_config, output_clients, sla_test_map)
            logging.info("[Done] sync test `" + test_name + "`")
            test_data = {}
            test_data.update(test_config['action'])
            test_data['sla_result'] = result
            test_data['sla_logs'] = logs
            test_data['index'] = index
            if 'suite' in test_config.keys():
                test_data['suite'] = test_config['suite']
            else:
                test_data['suite'] = "general"

            sla_results.append(test_data)

    if len(threads) > 0:
        print("Run in async mode...")
        signal.pause()
        for thread in threads:
            thread.join()
    else:
        logging.info("Run in sync mode, sending result...")
        logging.info(str(sla_results))

        sla_suite_summary = {}
        for sla_result in sla_results:
            suite = sla_result['suite']
            if suite not in sla_suite_summary.keys():
                sla_suite_summary[suite] = {
                    'success': 0,
                    'failed': 0,
                    'failed_details': []
                }

            if sla_result['sla_result'] == 0:
                sla_suite_summary[suite]['success'] = sla_suite_summary[suite]['success'] + 1
            else:
                sla_suite_summary[suite]['failed'] = sla_suite_summary[suite]['failed'] + 1
                sla_suite_summary[suite]['failed_details'].append(sla_result['sla_meta'])

        if "general_report_outputs" in sla_test_map.keys():
            for general_report_output in sla_test_map['general_report_outputs']:
                logging.info("Sending general reports on output " + general_report_output)
                client = output_clients[general_report_output]
                client.write_general_report(sla_results=sla_results, sla_suite_summary=sla_suite_summary)

def start_test_thread(test_name, test_config, output_clients, sla_test_map):
    """
    Starts  a test thread, used to perform SLA tests on a specific interval

    :param test_name:
    :param influxdb_client:
    :param run_function:
    :param data:
    :return:
    """
    try:
        logging.info("Starting async thread test %s" % test_name)

        test_thread = Thread(
            target=run_async_interval_test,
            args=(test_name, test_config, output_clients, sla_test_map)
        )
        test_thread.start()

        return test_thread

    except Exception as e:
        logging.error("Cannot start thread: %s" % e)
        sys.exit(1)


def run_async_interval_test(test_name, test_config, output_clients, sla_test_map):
    """
    Runs a single test

    :param test_name:
    :param test_config:
    :param output_clients:
    :param sla_test_map:
    :return:
    """
    global stop_program

    while True:
        if stop_program is True:
            logging.info("[%s] exited" % test_name)
            sys.exit(0)

        interval = sla_test_map['interval']
        if "interval" in test_config.keys():
            interval = test_config["interval"]

        run_sync_test(test_name, test_config, output_clients, sla_test_map)

        logging.info("Sleeping period " + str(interval))
        for i in range(1, interval):
            if stop_program is True:
                logging.info("[%s] exited" % test_name)
                sys.exit(0)
            sleep(1)
    #todo write test outupt to outputs


def run_sync_test(test_name, test_config, output_clients, sla_test_map):
    """
    Run test in sync mode

    :param test_name:
    :param test_config:
    :param output_clients:
    :param sla_test_map:
    :return:
    """
    result = 1
    logs = ""
    test_context = str(uuid.uuid4())[0:6] + "-" + test_name
    logging.info(test_context + "Starting test with context " + test_context)
    # print(str(test_config))

    try:
        sla_test_map_copy = sla_test_map.copy()
        test_config_copy = test_config.copy()
        external_variables = sla_test_map_copy["parameters"]
        result, logs = execute_action(
            test_context,
            test_name,
            test_config_copy['action']['description'],
            test_config_copy['action'],
            test_config_copy['action']['action_type'],
            external_variables,
            sla_test_map_copy
        )
    except Exception as e:
        logging.error(test_context + "#1 There was an exception while running tests: " + str(e) + str(traceback.format_exc()))
        result = 1
    finally:
        logging.info("Writting SLA test result to output")
        write_sla_results(result, output_clients, test_config, test_context)

    return result, logs


def write_sla_results(result, outputs, test_config, test_context):
    """
    Write tests results to outputs, all threads wil wait here in order to use a single connection for outputs like influx
    Implemented like this to be thread safe

    :param result:
    :param outputs:
    :param action_specs:
    :return:
    """
    mutex_output.acquire()
    try:
        if "outputs" in test_config.keys():
            for output_name in test_config['outputs']:
                try:
                    if output_name not in outputs.keys():
                        raise ValueError("Can not write output, output " + output_name + " is not found")
                    logging.info("Writing result output to " + output_name)
                    outputs[output_name].write_result(result, test_config['test_name'], test_context)
                except Exception as e:
                    logging.error("There was an exception while writing output: " + str(traceback.format_exc()))
                finally:
                    logging.info("Write output done")
    finally:
        mutex_output.release()

def execute_action(test_context, description, test_name, action_specs, action_type, external_variables, sla_test_map):
    """
    Runs a test action in a recursive mode

    :param description:
    :param test_name:
    :param action_specs:
    :param action_type:
    :param external_variables:
    :param sla_test_map:
    :return:
    """
    logging.info(test_context + " Execute action: " + description)
    result = None
    logs = ""

    if "time_parameters" in action_specs.keys():
        for time_var_name, time_var_def in action_specs["time_parameters"].items():
            logging.info(test_context + " Computing time parameter " + time_var_name)
            time_param_value = evaluate_time_parameter(time_var_def)
            external_variables[time_var_name] = time_param_value
            logging.info(test_context + " Computing time parameter as " + str(time_param_value))

    if action_type == "request":
        result, logs = sla_request(test_context, description, test_name, action_specs, action_type, external_variables, sla_test_map)
    elif action_type == "mysql_select":
        result, logs = sla_mysql(test_context, description, test_name, action_specs, action_type, external_variables, sla_test_map)
    elif action_type == "elasticsearch_query":
        result, logs = sla_elasticsearch_query(test_context,  description, test_name, action_specs, action_type, external_variables, sla_test_map)
    elif action_type == "ssl_expiration_date":
        result, logs = sla_ssl_expiration(test_context, description, test_name, action_specs, action_type, external_variables, sla_test_map)
    elif action_type == "rabbitmq_get_api_queues":
        result, logs = sla_rabbitmq_api_queue(test_context, description, test_name, action_specs, action_type, external_variables, sla_test_map)

    # generic vm checkpoints
    elif action_type == "ssh_remote_swap":
        result, logs = sla_ssh_remote_swap(test_context, description, test_name, action_specs, action_type, external_variables, sla_test_map)
    elif action_type == "ssh_remote_bunnyshell_agent":
        result, logs = sla_ssh_remote_bunnyshell_agent(test_context, description, test_name, action_specs, action_type, external_variables, sla_test_map)
    elif action_type == "ssh_remote_digital_ocean_agent":
        result, logs = sla_ssh_remote_digital_ocean_agent(test_context, description, test_name, action_specs, action_type, external_variables, sla_test_map)
    elif action_type == "ssh_uptime_more_then_x_days":
        result, logs = sla_ssh_uptime_more_then_x_days(test_context, description, test_name, action_specs, action_type, external_variables, sla_test_map)
    elif action_type == "ssh_log_rotation_enabled":
        result, logs = sla_ssh_log_rotation_enabled(test_context, description, test_name, action_specs, action_type, external_variables, sla_test_map)

    # varnish metrics based on influx agregator with netdata
    elif action_type == "sla_varnish_influx_netdata_failed_backend_connections":
        result, logs = sla_varnish_influx_netdata_failed_backend_connections(test_context, description, test_name, action_specs, action_type, external_variables, sla_test_map)
    else:
        raise ValueError(test_name + " -> ERR Action type " + action_type + " is not recognized")

    if result != 0:
        return result, logs

    if "action" in action_specs.keys():
        logging.info(test_context + " -> continue with next action in line")
        return execute_action(
            test_context,
            test_name,
            action_specs['action']['description'],
            action_specs['action'],
            action_specs['action']['action_type'],
            external_variables,
            sla_test_map
        )
    else:
        return result, logs

if __name__ == "__main__":
    # signal.signal(signal.SIGINT, receive_signal)
    # signal.signal(signal.SIGTERM, receive_signal)
    # signal.signal(signal.SIGTSTP, receive_signal)
    #
    main()

    # message = b"This is the message to be encrypted"
    # with open('/Users/ingres/.ssh/id_rsa') as f:
    #     private_key_content = f.read()
    #     rsa.PrivateKey.load_pkcs1(private_key_content)
    #     with open('/Users/ingres/.ssh/id_rsa.pub') as g:
    #         public_key_content = g.read()
    #         pem_prefix = '-----BEGIN RSA PUBLIC KEY-----\n'
    #         pem_suffix = '-----END RSA PUBLIC KEY-----'
    #         key = '{}{}{}'.format(pem_prefix, public_key_content, pem_suffix)
    #         print(key)
    #         rsa.PublicKey.load_pkcs1(key)
    #         # print("loaded private key and private")


