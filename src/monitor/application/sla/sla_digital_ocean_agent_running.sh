#/usr/bin/env bash
set -e

output=`ps aux | grep do-agent | grep '/opt/digitalocean/bin/do-agent --syslog' | wc -l`
if [ $output -eq 1 ]; then
  echo "[sla_ok] Digital Ocean agent running ok"
else
  echo "[sla_failed] Digital Ocean agent not running"
fi