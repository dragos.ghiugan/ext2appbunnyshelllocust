#/usr/bin/env bash
echo "Using ssh private key $4"
ssh-add $4


function execute_ssh_sla_check() {
    echo " "
    echo "Perform $2 for host $1"
    echo " "

    ssh $1 "test -e /tmp/bunnyshell_automation_dist"
    if [ $? -eq 0 ]; then
        echo "automation scripts exists"
    else
        echo "automation scripts does not exists"
        scp -r sla_dist $1:/tmp/bunnyshell_automation_dist
        echo "Copy done"
    fi

    check_output=`ssh -i  $4 -o StrictHostKeyChecking=no -o LogLevel=ERROR -o ConnectTimeout=3  -o BatchMode=yes $1 "$3" $5 $6`
    echo ">>>--------- $2 --------"
    echo $check_output
    echo "<<<--------- >>> $2 --------"

    if [ $? -eq 0 ]; then
        echo "Script run without errors";
    else
        echo "Swap checking issues had errors on executing"
    fi

    sla_ok=`echo $check_output | grep "sla_ok" | wc -l`

    if [ $sla_ok -gt 0 ]; then
        echo "---sla_result_start---SLA_OK---sla_result_end---"
    else
        echo "---sla_result_start---SLA_NOT_OK---sla_result_end---"
    fi
}

execute_ssh_sla_check $1 $2 $3 $4 $5 $6