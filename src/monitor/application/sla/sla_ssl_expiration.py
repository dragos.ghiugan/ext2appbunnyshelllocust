# coding=utf-8
from subprocess import Popen, PIPE, STDOUT
import os
import logging
import sys
from influxdb import InfluxDBClient
import requests
import yaml
from datetime import datetime
from threading import Thread
from time import sleep
import signal
import json
import mysql.connector
from mysql.connector import Error
from requests.auth import HTTPBasicAuth
import pprint
import datetime
from datetime import timedelta
from pytz import timezone
from src.variables_crapper import identify_external_variables, extract_variables, in_place_identify_external_variables
import random
from urllib.parse import urlparse
from jsonschema import validate
import glob
import ssl
import socket
from src.monitor.application.sla_functions import verify_mandatory_key_exists, evaluate_time_parameter, replace_object_parameters, ssl_expiry_datetime


def sla_ssl_expiration(test_context, description, test_name, action_specs, action_type, external_variables, sla_test_map):
    action_config_mandatory_keys = ["domains", "timeout"]
    verify_mandatory_key_exists(action_config_mandatory_keys, action_specs)

    domains = action_specs['domains']
    now = datetime.datetime.now()
    for domain_name, before_expiration_days in domains.items():
        expiration_date = ssl_expiry_datetime(domain_name)
        delta = expiration_date - now
        logging.info(test_context + " Domain " + domain_name + " - days before expiration " + str(delta.days))
        if delta.days < before_expiration_days:
            raise ValueError("The expiration date test failed, domain " + domain_name + " expires in " + str(delta.days) + " days")

    return 0, ""