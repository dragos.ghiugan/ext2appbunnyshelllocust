#/usr/bin/env bash
if [ -d "dist" ]; then
    echo "dist directory found, recreating it"
    rm -rf dist
    mkdir dist
else
    echo "dist directory not found"
    mkdir dist
fi

if [ -d "sla_dist" ]; then
    echo "dist directory found, recreating it"
    rm -rf sla_dist
    mkdir sla_dist
else
    echo "sla_dist directory not found"
    mkdir sla_dist
fi

echo "Copy sla files to dist folder"
cp src/monitor/application/sla/sla* sla_dist/
pyinstaller  --onefile src/monitor/application/sla/sla_mysql.py
cp dist/sla_mysql sla_dist/