# coding=utf-8
from subprocess import Popen, PIPE, STDOUT
import os
import logging
import sys
from influxdb import InfluxDBClient
import requests
import yaml
from datetime import datetime
from threading import Thread
from time import sleep
import signal
import json
import mysql.connector
from mysql.connector import Error
from requests.auth import HTTPBasicAuth
import logging
import datetime
from datetime import timedelta
from pytz import timezone
from src.variables_crapper import identify_external_variables, extract_variables, in_place_identify_external_variables
import random
from jsonschema import validate
import glob
import ssl
import socket
from src.monitor.application.sla_functions import verify_mandatory_key_exists, evaluate_time_parameter, replace_object_parameters, ssl_expiry_datetime, read_config
from subprocess import Popen, PIPE, STDOUT

SLA_VALUE_ERROR = "SLA_VALUE_ERROR"

def extract_result_from_process_output(output_process):
    """
    Extracts the result from output

    :param output_process:
    :return:
    """
    if output_process is None:
        raise ValueError("Can ont extract any result, output is None")
    if not output_process:
        raise ValueError("Can ont extract any result, empty output")

    if "---sla_result_start---" in output_process and "---sla_result_end---" in output_process:
        begin_result = output_process.split('---sla_result_start---')[1]
        result = begin_result.split('---sla_result_end---')[0]
        return result
    else:
        raise ValueError("Can ont extract any result, result marks not found")


def ssh_exec_file_remotly(ssh_host, ssh_user, ssh_key, script_file_path, arguments = []):
    """
    Exec bash file remote

    :param arguments:
    :param ssh_host:
    :param ssh_user:
    :param ssh_key:
    :param script_file_path:
    :return:
    """
    command = [
        "bash",
        "sla_exec_ssh.sh",
        ssh_user + "@" + ssh_host,
        "SLA_ROBOT",
        script_file_path,
        ssh_key
    ]
    for argument in arguments:
        command.append(argument)

    # 4 arguments are used to setup the ssh connection and specify script
    # the rest of 10 argument are used to send them to bash and be used for processing
    # if no argument sent then fill with 0 up to 10 more arguments to be consistent
    # if you need more then 10 arguments at some point, then do the padding  to new required number with 0
    if len(command) < 14:
        while len(command) < 14:
            command.append("0")

    logging.info("Execute command: " + " ".join(command))

    out = Popen(command, stderr=STDOUT, stdout=PIPE)

    output = out.communicate()[0]
    return_code = out.returncode

    logging.info(str(output))
    if return_code != 0:
        logging.info("SSh output is not ok: " + str(return_code) + " out: " + str(output))

        return SLA_VALUE_ERROR, str(output)

    sla_result = extract_result_from_process_output(str(output))
    logging.info("SLA Result is: " + str(sla_result))

    if sla_result == "SLA_OK":
        return 0, str(output)

    return 1, str(output)


def sla_ssh_remote_swap(test_context, description, test_name, action_specs, action_type, external_variables, sla_test_map):
    action_config_mandatory_keys = [
        "action_type",
        "ssh_host",
        "ssh_rsa_private_key",
        "ssh_user"
    ]
    verify_mandatory_key_exists(action_config_mandatory_keys, action_specs)

    ssh_host = action_specs["ssh_host"]
    ssh_rsa_key = action_specs["ssh_rsa_private_key"]
    ssh_user = action_specs["ssh_user"]

    ssh_rsa_key = in_place_identify_external_variables(ssh_rsa_key, external_variables)
    ssh_user = in_place_identify_external_variables(ssh_user, external_variables)

    return ssh_exec_file_remotly(
        ssh_host,
        ssh_user,
        ssh_rsa_key,
        "./sla_swap.sh"
    )


def sla_ssh_remote_bunnyshell_agent(test_context, description, test_name, action_specs, action_type, external_variables, sla_test_map):
    action_config_mandatory_keys = ["action_type", "ssh_host", "ssh_rsa_private_key", "ssh_user"]
    verify_mandatory_key_exists(action_config_mandatory_keys, action_specs)

    ssh_host = action_specs["ssh_host"]
    ssh_rsa_key = action_specs["ssh_rsa_private_key"]
    ssh_user = action_specs["ssh_user"]

    ssh_rsa_key = in_place_identify_external_variables(ssh_rsa_key, external_variables)
    ssh_user = in_place_identify_external_variables(ssh_user, external_variables)

    return ssh_exec_file_remotly(
        ssh_host,
        ssh_user,
        ssh_rsa_key,
        "./sla_bunnyshell_agent.sh"
    )


def sla_ssh_remote_digital_ocean_agent(test_context, description, test_name, action_specs, action_type, external_variables, sla_test_map):
    action_config_mandatory_keys = ["action_type", "ssh_host", "ssh_rsa_private_key", "ssh_user"]
    verify_mandatory_key_exists(action_config_mandatory_keys, action_specs)

    ssh_host = action_specs["ssh_host"]
    ssh_rsa_key = action_specs["ssh_rsa_private_key"]
    ssh_user = action_specs["ssh_user"]

    ssh_rsa_key = in_place_identify_external_variables(ssh_rsa_key, external_variables)
    ssh_user = in_place_identify_external_variables(ssh_user, external_variables)

    return ssh_exec_file_remotly(
        ssh_host,
        ssh_user,
        ssh_rsa_key,
        "./sla_digital_ocean_agent_running.sh"
    )


def sla_ssh_uptime_more_then_x_days(test_context, description, test_name, action_specs, action_type, external_variables, sla_test_map):
    action_config_mandatory_keys = ["action_type", "ssh_host", "ssh_rsa_private_key", "ssh_user"]
    verify_mandatory_key_exists(action_config_mandatory_keys, action_specs)

    ssh_host = action_specs["ssh_host"]
    ssh_rsa_key = action_specs["ssh_rsa_private_key"]
    ssh_user = action_specs["ssh_user"]

    ssh_rsa_key = in_place_identify_external_variables(ssh_rsa_key, external_variables)
    ssh_user = in_place_identify_external_variables(ssh_user, external_variables)

    return ssh_exec_file_remotly(
        ssh_host,
        ssh_user,
        ssh_rsa_key,
        "./sla_uptime.sh"
    )

def sla_ssh_log_rotation_enabled(test_context, description, test_name, action_specs, action_type, external_variables, sla_test_map):
    action_config_mandatory_keys = ["action_type", "ssh_host", "ssh_rsa_private_key", "ssh_user"]
    verify_mandatory_key_exists(action_config_mandatory_keys, action_specs)

    ssh_host = action_specs["ssh_host"]
    ssh_rsa_key = action_specs["ssh_rsa_private_key"]
    ssh_user = action_specs["ssh_user"]

    ssh_rsa_key = in_place_identify_external_variables(ssh_rsa_key, external_variables)
    ssh_user = in_place_identify_external_variables(ssh_user, external_variables)

    return ssh_exec_file_remotly(
        ssh_host,
        ssh_user,
        ssh_rsa_key,
        "./sla_log_rotation.sh"
    )


def sla_varnish_influx_netdata_failed_backend_connections(test_context, description, test_name, action_specs, action_type, external_variables, sla_test_map):
    action_config_mandatory_keys = ["action_type", "ssh_host", "ssh_rsa_private_key", "ssh_user"]
    verify_mandatory_key_exists(action_config_mandatory_keys, action_specs)

    ssh_host = action_specs["ssh_host"]
    ssh_rsa_key = action_specs["ssh_rsa_private_key"]
    ssh_user = action_specs["ssh_user"]

    ssh_rsa_key = in_place_identify_external_variables(ssh_rsa_key, external_variables)
    ssh_user = in_place_identify_external_variables(ssh_user, external_variables)

    return ssh_exec_file_remotly(
        ssh_host,
        ssh_user,
        ssh_rsa_key,
        "./sla_varnish_influx_netdata_failed_backend_connections.sh"
    )
#
# def main():
#     logging.info("Starting experimental sla")
#
#     virtual_machines=["159.65.192.138", "159.65.198.128", "167.71.68.41", "165.22.196.196", "104.248.84.147", "134.209.200.184", "142.93.237.106", "64.225.68.162", "64.225.71.59", "134.122.49.89", "157.245.72.172", "165.22.194.123", "157.245.66.101", "164.90.202.210", "188.166.17.107", "178.62.249.247"]
#
#     web_servers=("188.166.110.109", "188.166.113.107", "167.71.8.177", "157.245.71.217", "188.166.26.27", "161.35.85.78", "64.227.78.110", "142.93.143.129", "161.35.86.34", "167.71.13.212")
#
#     influx_virtual_machines="157.245.72.172"
#
#     dict_result = {}
#     execute_generic_vm_checkpoints(
#         "157.245.72.172",
#         "bunnyshell",
#         "/Users/ingres/.ssh/id_rsa",
#         dict_result
#     )
#
#     logging.info(dict_result)
#
# main()
