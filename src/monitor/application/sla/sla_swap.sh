#/usr/bin/env bash
set -e \
&& swapCached=`cat /proc/meminfo | grep -i swap | head -n 1 | cut -d":" -f 2 | cut -d"k" -f 1 | xargs printf` \
&& echo "Swap cached is (kb): $swapCached" \
&& swapTotal=`cat /proc/meminfo | grep -i swap | head -n 2 | tail -n 1 | cut -d":" -f 2 | cut -d"k" -f 1 | xargs printf` \
&& echo "Swap total is (kb): $swapTotal" \
&& swapFree=`cat /proc/meminfo | grep -i swap | head -n 3 | tail -n 1 | cut -d":" -f 2 | cut -d"k" -f 1 | xargs printf`\
&& echo "Swap free is (kb): $swapFree" \
&& if [ $swapTotal -eq 0 ]; then \
    echo "[sla_failed] SwapTotal is zero!"; \
    exit 1; \
fi
if [ $swapFree -eq 0 ]; then
    if [ -z $1 ]; then
      echo "Allowing swap total to be 0 kb"
    else
      if [ $1 -eq 0 ]; then
        echo "Allowing swap total to be 0 kb"
      else
        echo "[sla_failed] Swap Free is zero!!";
        exit 1;
      fi
    fi
fi

echo "[sla_ok] Swap memory is ok"
