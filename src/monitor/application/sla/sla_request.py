# coding=utf-8
from subprocess import Popen, PIPE, STDOUT
import os
import logging
import sys
from influxdb import InfluxDBClient
import requests
import yaml
from datetime import datetime
from threading import Thread
from time import sleep
import signal
import json
import mysql.connector
from mysql.connector import Error
from requests.auth import HTTPBasicAuth
import pprint
import datetime
from datetime import timedelta
from pytz import timezone
from src.variables_crapper import identify_external_variables, extract_variables, in_place_identify_external_variables
import random
from urllib.parse import urlparse
from jsonschema import validate
import glob
import ssl
import socket
from src.monitor.application.sla_functions import verify_mandatory_key_exists, evaluate_time_parameter, replace_object_parameters, ssl_expiry_datetime, make_request


def sla_request(test_context, description, test_name, action_specs, action_type, external_variables, sla_test_map):
    logging.info(test_context + " Execute Request action " + action_type)
    action_config_mandatory_keys = ["type", "url", "verify", "timeout"]
    verify_mandatory_key_exists(action_config_mandatory_keys, action_specs)

    response = make_request(action_specs, external_variables)

    if "extractors" in action_specs.keys():
        extract_variables(action_specs['extractors'], response, extracted_variables=external_variables)

    logging.info(test_context + " Response status code:" + str(response.status_code))
    # logging.info(test_context + " External variables:" + str(external_variables))

    has_assertions = False
    if "assertions" in action_specs.keys():
        logging.info(test_context + " Asserting...")

        if "json_schema" in action_specs['assertions'].keys():
            json_response = response.json()
            # logging.info(str(json_response))
            validate(instance=json_response, schema=action_specs['assertions']['json_schema'])
            logging.info(test_context + " Assertions json_schema validated")
            has_assertions = True

        if "variables_json_schema" in action_specs['assertions'].keys():
            validate(instance=external_variables, schema=action_specs['assertions']['variables_json_schema'])
            logging.info(test_context + " Assertions variables_json_schema validated")
            has_assertions = True

        if "html" in action_specs['assertions'].keys():
            page_html = response.content.decode("UTF-8")

            for html_assertion in action_specs['assertions']['html']:
                if not isinstance(html_assertion, dict):
                    raise ValueError("The html assertion item should a dict")
                if "contains" in html_assertion.keys():
                    has_assertions = True
                    if html_assertion['contains'] not in page_html:
                        raise ValueError("The page html does not contains " + str(html_assertion['contains']))
                if "not_contains" in html_assertion.keys():
                    has_assertions = True
                    if isinstance(html_assertion['not_contains'], str):
                        if html_assertion['not_contains'] in page_html:
                            raise ValueError("The page html contains " + str(html_assertion['not_contains']))
                    elif isinstance(html_assertion['not_contains'], list):
                        for not_contain_text in html_assertion['not_contains']:
                            if not_contain_text in page_html:
                                raise ValueError("The page html contains " + str(not_contain_text))
                    else:
                        raise ValueError("The not_contains must have as values list or string")
            logging.info(test_context + " Assertions html validated")
        if not has_assertions:
            raise ValueError("Each test must have assertions in order to validate that test was OK")

    return 0, ""