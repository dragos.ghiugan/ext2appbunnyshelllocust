import logging
from gunicorn.app.base import Application, Config
import gunicorn
from gunicorn import glogging
from gunicorn.workers import sync
from flask import Flask, request, abort, has_request_context
from flask_jsonpify import jsonpify
import multiprocessing
import urllib3
import sys
import os
import grp
import pwd
from subprocess import Popen, PIPE, STDOUT
import requests
import json
import time
# from sempahore import OperationsSemaphore
from flask_request_id_header.middleware import RequestID
import uuid
import threading

@app.route('/get_grafana_notification', methods=['POST'])
def get_grafana_notification():
    logging.info("Called  grafana notification")
    try:
        logging.info("Called  grafana notification for alarm with id " + str(request.json.get('ruleName')))
        logging.info(str(request.json))

        value = 0
        is_nodata = False

        eval_matches = request.json.get('evalMatches')
        tags = None

        if len(eval_matches) > 0:
            if 'value' in eval_matches[0]:
                value = eval_matches[0]['value']
            else:
                is_nodata = True

            if 'tags' in eval_matches[0]:
                tags = eval_matches[0]['tags']
            else:
                tags = None
        else:
            is_nodata = True

        timestamp = int(time.time())
        alarm_context = str(uuid.uuid4())
        logging.info("Alarm context for dashboard is " + str(alarm_context))

        data = {
            'alarm_id' : request.json.get('ruleName'),
            'state' : request.json.get('state'),
            'value' : value,
            'timestamp': timestamp,
            'alarm_context': alarm_context
        }

        try:
            if tags:
                logging.info("!!! host tag val is " + tags['host'])
            else:
                logging.info("!!! tags is NULL")

        except Exception as e:
            logging.info("!!! error: %s" % e, exc_info=True)

        if is_nodata:
            if data['state'] == 'alerting':
                data['state'] = 'no_data'

        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }

        url = '%s/alarm/set-alarm-status' % bunnyshell_api

        logging.info("Using url %s" % url)
        response = make_request(url, 'post', data, headers)

        logging.info(response)

        return jsonpify("Success!")
    except Exception as e:
        logging.info("There was an error sending alert to dashboard: " + str(e), exc_info=True)
        return jsonpify("Error!")


def make_request(url, request_type, data, headers):

    response_data = None
    r = None

    logging.info(str(headers))
    try:

        if request_type == 'post':
            logging.info("Sending dashboard request to url " + url + " with data: " + str(data))
            r = requests.post(url, json=data, headers=headers)
        elif request_type == 'get':
            logging.info("Sending dashboard request with data: " + str(data))
            r = requests.get(url, params=data, headers=headers)
        elif request_type == "delete":
            logging.info("Sending dashboard request to url " + url)
            r = requests.delete(url, headers=headers)
        else:
            logging.error("Request type %s is not supported" % request_type)
            return None

        r.raise_for_status()

    except Exception as e:

        text = 'none'
        if r:
            text = r.text

        logging.error("An error occured during request: %s. Text: %s" % (e, text), exc_info=True)

        try:
            logging.error("Response text: %s." % e.response.text)
        except Exception as e:
            pass

        try:
            logging.error("Response content: %s." % e.response.content)
        except Exception as e:
            pass

        return None

    if r.status_code == 404:
        logging.error("Dashboard not found, status code 404")
        return None

    if r.status_code != 200:
        logging.error("Response code was %d: %s. Message: %s" % (r.status_code, r.reason))
        return None

    try:
        response_data = json.loads(r.text)

    except Exception as e:
        logging.error("Request response is not a valid json: %s" % e, exc_info=True)
        return None

    logging.info("Dashboard was found")

    return response_data


def get_dashboard(dashboard_uid):

    logging.info("Getting dashboard with id %s" % dashboard_uid)

    headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + grafana_token
    }

    url = grafana_url + '/dashboards/uid/' + dashboard_uid

    response = make_request(url, 'get', data={}, headers=headers)

    return response


def grafana_get_alarms():

    logging.info("Getting all alarms" )

    headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + grafana_token
    }

    url = grafana_url + '/alarms/'

    response = make_request(url, 'get', data={}, headers=headers)

    return response


def grafana_search_all(page, limit):

    logging.info("Getting all alarms" )

    headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + grafana_token
    }

    url = grafana_url + '/search?query=%&page=' + str(page) + '&limit=' + str(limit)

    response = make_request(url, 'get', data={}, headers=headers)

    return response


def grafana_get_all_folders():

    logging.info("Getting all folders")

    headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + grafana_token
    }

    url = grafana_url + '/folders'

    response = make_request(url, 'get', data={}, headers=headers)

    return response



def update_dashboard(data):
    logging.info("Updating dashboard")
    headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + grafana_token
    }

    url = grafana_url + '/dashboards/db'

    response = make_request(url, 'post', data, headers)

    logging.info("Response is %s" % response)

    if response["status"] != "success":
        logging.error("Update dashboard failed with %s" % str(response))
        return 1

    return 0

def remove_dashboard(dashboard_uid):
    logging.info("deleting dashboard")
    headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + grafana_token
    }

    url = grafana_url + '/dashboards/uid/' + dashboard_uid

    response = make_request(url, 'delete', {}, headers)

    logging.info("Response is %s" % response)

    return 0

def get_charts(charts, chart_ids):

    found_charts = []

    for chart in charts:
        if chart['id'] in chart_ids:
            found_charts.append(chart)

    return found_charts

def get_chart(charts, chart_id):

    for chart in charts:
        if str(chart['id']) == str(chart_id):
            return chart

    return None

def delete_charts(charts, chart_ids):
    """
    Delete charts, number are ints

    """
    new_charts = []
    index = 0

    for chart in charts:
        logging.info("Checking dashboard chart with id %s" % str(chart['id']))

        if int(chart['id']) not in chart_ids:
            chart['gridPos']['y'] = index * 7 # chart height
            new_charts.append(chart)
            index += 1
        else:
            logging.info("This chart will be removed")

    return new_charts

def get_grafana_alert(alarm_id, interval, comparison_term, metric_aggregation, alarm_value, period_count, metric):

    alert = None

    alarm_id = int(alarm_id)
    interval = int(interval)
    period_count = int(period_count)
    alarm_value = int(alarm_value)

    try:

        logging.info("Creating alert %d" % alarm_id)
        logging.info("Interval %d" % interval)

        try:
            with open(alert_template) as json_file:
                alert = json.load(json_file)
        except Exception as e:
            logging.error("Cannot read alert template: %s" % e, exc_info=True)
            return None

        condition = alert["conditions"][0]

        condition["evaluator"]["params"] = [alarm_value]
        condition["evaluator"]["type"] = comparison_term
        condition["query"]["params"] = ["A", "120s", "now"]

        if 'elasticsearch' in metric:
            condition["query"]["params"] = ["A", "1m", "now"]

        condition["reducer"]["type"] = metric_aggregation

        alert["conditions"] = [condition]

        alert["executionErrorState"] = "keep_state"
        alert["frequency"] = str(interval) + "s"
        alert["message"] = '{"alarm_id": %d}' % alarm_id
        alert["name"] = str(alarm_id)

        for_period = interval * (period_count - 1)
        if for_period > 10:
            for_period -= 10

        alert["for"] = str(for_period) + "s"

        alert["noDataState"] = "alerting"
        alert["notifications"] = [{"id": 1}]

    except Exception as e:
        logging.info("Cannot create alarm configuration %s" % e, exc_info=True)
        return None

    return alert


def add_chart(chart_nr, chart_id, metric, environment_name):

    chart = None

    logging.info("Creating chart on position %d" % (chart_nr * 7))

    try:
        with open(chart_template) as json_file:
            chart = json.load(json_file)
    except Exception as e:
        logging.error("Cannot read chart template: %s" % e, exc_info=True)
        return None

    select = None

    if metric == 'netdata.system.cpu.user':
        metric = 'netdata.system.cpu.idle'
        select = { "params": ["*-1 + 100"], "type": "math" }

    chart['gridPos']['y'] = chart_nr * 7 # chart height
    chart['id'] = chart_id
    chart['targets'][0]['alias'] = '$tag_host'
    chart['targets'][0]['measurement'] = metric
    chart['targets'][0]['tags'][0]['operator'] = '=~'
    chart['targets'][0]['tags'][0]['value'] = '/' + environment_name + '_.*/'
    chart['title'] = environment_name + ' - ' + metric

    if select:
        chart['targets'][0]['select'][0].append(select)

    return chart

def remove_all_alarms(owner_id):

    try:

        dashboard_title = str(owner_id)
        dashboard_uid = '%s-alarms' % owner_id

        logging.info(str(dashboard_uid))

        dashboard_json = get_dashboard(dashboard_uid)

        if dashboard_json is None:
            logging.info("Cannot find dashboard")
            return 0

        if 'panels' not in dashboard_json['dashboard']:
            return 0

        dashboard_json['dashboard']['panels'] = []

        result = update_dashboard(dashboard_json)

        if result != 0:
            return 1

        logging.info("Removed all alarm")

    except Exception as e:
        logging.error("An error occurred while removing all alarms: %s" % e, exc_info=True)
        return 1

    return 0

