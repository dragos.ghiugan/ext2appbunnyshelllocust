from Command import Command
import argparse
from src.monitor.application.grafana_factory import GrafanaFactory
from src.monitor.application.grafana_service import GrafanaService
from src.monitor.application.context_service import ContextService
from src.monitor.application.file_service import FileService
import requests
import json
import sys
import os


class BuildMonitoringCommand(Command):

    name = "app:monitoring:build"
    description = """
    
    Build grafana charts, builds a single config file which contains all settings for a single definition
    Build config files for each definition
    
     eg: 
     
python src/monitor/presentation/command/console.py app:monitoring:build  --sla-dir /Users/ingres/work/bunnyshell/x1_project/sla_specs/definition --build-dir /Users/ingres/work/bunnyshell/x1_project/sla_specs/build

    """

    def __init__(self, parameters) -> None:
        """
        Complex commands can accept one or several receiver objects along with
        any context data via the constructor.
        """
        self.parameters = parameters
        self.parser = argparse.ArgumentParser(description=self.description)
        self.parser.add_argument('--sla-dir', nargs='?', help='SLA directory with monitoring configuraiton', required=True)
        self.parser.add_argument('--build-dir', nargs='?', help='SLA directory where to set the build configuration', required=True)

    def execute(self) -> None:
        parse_args = sys.argv[2:]
        arguments = vars(self.parser.parse_args(args=parse_args))
        sla_dir = arguments['sla_dir']
        build_dir = arguments['build_dir']
        if not os.path.isdir(sla_dir):
            raise ValueError(str(sla_dir) + " is not a directory")
        print("Compiling charts for sla directory " + sla_dir)

        for node in os.listdir(sla_dir):
            sla_dir_node = sla_dir + "/" + node
            if os.path.isdir(sla_dir_node) and not node.startswith("."):
                print("compiling for node " + sla_dir_node)
                stress_test_map = self.factory_sla_tests_from_dir(sla_dir=sla_dir_node)

                if "grafana_uid" not in stress_test_map.keys():
                    raise ValueError("Grafana uid not specified in stress test map")

                if "grafana_influx_name" not in stress_test_map.keys():
                    raise ValueError('Grafana influx data source not specified test map')

                grafana_api_key = self.parameters["grafana_api_key"]
                grafana_service = GrafanaService(
                    grafana_api_key=grafana_api_key,
                    grafana_url=self.parameters["grafana_hostname"]
                )
                dashboard_info = grafana_service.get_dashboard_by_uid(stress_test_map['grafana_uid'])

                dashboard_model = dashboard_info['dashboard']
                dashboard_model['panels'] = []

                print("Creating grafana charts...")
                for sla_test_name, sla_test_config in stress_test_map['sla_tests'].items():
                    dashboard_model['panels'].append(
                        GrafanaFactory.factory_monitoring_panel(
                            len(dashboard_model['panels']) + 1,
                            sla_test_name,
                            stress_test_map['grafana_influx_name'],
                            "sla." + sla_test_name)
                    )

                    print(sla_test_name)
                # # dashboard_model['panels'] = []
                dashboard_update_payload = {
                    "dashboard": dashboard_model,
                    "overwrite": True
                }

                print("Updating grafana charts...")
                grafana_service.update_dashboard_json(dashboard_update_payload)

                build_file_name = "build_" + node + ".yml"
                build_file_path = build_dir + "/" + build_file_name

                print("Create build configuration in " + build_file_path)
                with open(build_file_path, 'w') as fp:
                    json.dump(stress_test_map, fp, indent=4)

        # print(str())
        # read entire directory sla

    def factory_sla_tests_from_dir(self, sla_dir):
        """
        Parse a sla directory and construct stress test map

        :param sla_dir:
        :return:
        """
        if not os.path.isdir(sla_dir):
            raise ValueError("Config dir not defined")

        config_file = sla_dir + "/config.yml"
        parameters_file = sla_dir + "/parameters.yml"
        if not os.path.isfile(config_file):
            raise ValueError("Config file not found at " + config_file)

        config_data = FileService.read_yml_or_dict(config_file)
        sla_test_map = {
            'sla_tests': {}
        }
        sla_test_map.update(config_data)

        if not "parameters" in sla_test_map.keys():
            sla_test_map['parameters'] = {}

        if os.path.isfile(parameters_file):
            parameters_data = FileService.read_yml_or_dict(parameters_file)
            sla_test_map['parameters'].update(parameters_data)

        for file_name in os.listdir(sla_dir):
            if file_name.startswith("sla_"):
                spec_data = FileService.read_yml_or_dict(sla_dir + "/" + file_name)

                print(" -> Loading test " + str(sla_dir + "/" + file_name))
                if "test_name" in spec_data.keys():
                    test_name = spec_data['test_name']
                else:
                    test_name = ""

                test_name_by_file = os.path.splitext(file_name)[0]
                if len(test_name) == 0 or test_name == "":
                    test_name = test_name_by_file
                print("Test name computed is  " + str(test_name))

                if spec_data['is_enabled']:
                    spec_data['test_name'] = test_name
                    sla_test_map['sla_tests'][test_name] = spec_data
                else:
                    print(test_name + " - test not enabled: ")

        if sla_test_map is None:
            raise ValueError("The stress test map is none")

        return sla_test_map
