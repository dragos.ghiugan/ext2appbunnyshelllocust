from Command import Command
import argparse
from src.monitor.application.grafana_factory import GrafanaFactory
from src.monitor.application.grafana_service import GrafanaService
import requests
import json
import sys
import os


class FactoryBasicMonitoringCommand(Command):

    name = "app:monitoring:factory:basic-monitoring"
    description = "Factory expert beauty proactive monitoring system"

    """
    e.g:
    
python src/monitor/presentation/command/console.py app:monitoring:factory:basic-monitoring --definition-dir /Users/ingres/work/bunnyshell/x1_project/sla_specs/definition/expert_beauty
    
    """

    def __init__(self, parameters) -> None:
        """
        Complex commands can accept one or several receiver objects along with
        any context data via the constructor.
        """
        self.parameters = parameters
        self.parser = argparse.ArgumentParser(description=self.description)
        self.parser.add_argument('--definition-dir', nargs='?', help='Path where to dump the config file', required=True)


    def execute(self) -> None:
        """
        Commands can delegate to any methods of a receiver.
        """
        parse_args = sys.argv[2:]
        arguments = vars(self.parser.parse_args(args=parse_args))
        definition_dir = arguments['definition_dir']

        if not os.path.isdir(definition_dir):
            raise ValueError("Definition dir not found")

        bunnyshell_api_token = self.parameters["bunnyshell_api_token"]
        vm_list = self.get_bunnyshell_vm_list(bunnyshell_api_token=bunnyshell_api_token)
        vms = vm_list['vms']

        grafana_api_key = self.parameters["grafana_api_key"]
        grafana_service = GrafanaService(
            grafana_api_key=grafana_api_key,
            grafana_url=self.parameters["grafana_hostname"]
        )

        monitoring_config = self.create_basic_monitoring_sla_configs(vms)

        print("Dump json")
        with open(definition_dir + "/config.yml", 'w') as fp:
            json.dump(monitoring_config, fp, indent=4)

    def get_bunnyshell_vm_list(self, bunnyshell_api_token):
        """
        Returns bunnyshell vm instance list

        :param bunnyshell_api_token:
        :return:
        """
        url = "https://cloud.bunnyshell.com/v1/organization/me"

        payload={}
        headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + bunnyshell_api_token,
        }

        response = requests.request("GET", url, headers=headers, data=payload)
        if response.status_code != 200:
            raise ValueError("Status code is not 200")
        decoded_content = response.content.decode("utf-8")

        return json.loads(decoded_content)


    def create_basic_monitoring_sla_configs(self, vms):
        sla_test_map = {
            "run_mode": "not_async_interval",
            "interval": 10,
            "logging_file_path": '/var/log/bunnyshell/sla_bunnyshell.log',
            "grafana_uid": "-SST3RCGz",
            "grafana_influx_name": "InfluxDB_XpertBeauty",
            "outputs": {
                "result_file": {
                    "type": "file",
                    "path": "/var/log/bunnyshell/sla_result_output.log"
                },
                "storage_influx_1": {
                    "type": "influx",
                    "measurement_prefix": "sla",
                    "influx_host": "{{ influx_host }}",
                    "influx_port": "{{ influx_port }}",
                    "influx_database": "{{ influx_db }}",
                    "influx_user": "{{ influx_user }}",
                    "influx_password": "{{ influx_password }}",
                }
            },
            "sla_tests": {}
        }

        #
        # Generic VM Checkpoints
        #
        for vm_instance in vms:
            public_ip = None
            if "VM public ip" in vm_instance.keys():
                public_ip = vm_instance["VM public ip"]

            if "WS instance public ip" in vm_instance.keys():
                public_ip = vm_instance["WS instance public ip"]

            # Swap test
            test_name = "Swap " + public_ip
            tst_config = {
                "test_name": test_name,
                "is_enabled": False,
                "required_parameters": [],
                "outputs": ["storage_influx_1", "result_file"],
                "suite": "Swap",
                "action": {
                    "description": "Test " + public_ip,
                    "action_type": "ssh_remote_swap",
                    "ssh_host": public_ip,
                    "ssh_rsa_private_key": "{{ ssh_key_path }}",
                    "ssh_user": "{{ ssh_user }}",
                    "sla_meta": vm_instance
                }
            }
            sla_test_map['sla_tests'][test_name] = tst_config

            # Bunnyshell Agent test
            test_name = "Bunnyshell Agent " + public_ip
            tst_config = {
                "test_name": test_name,
                "is_enabled": False,
                "required_parameters": [],
                "outputs": ["storage_influx_1", "result_file"],
                "suite": "Bunnyshell Agent",
                "action": {
                    "description": "Test " + public_ip,
                    "action_type": "ssh_remote_bunnyshell_agent",
                    "ssh_host": public_ip,
                    "ssh_rsa_private_key": "{{ ssh_key_path }}",
                    "ssh_user": "{{ ssh_user }}",
                    "sla_meta": vm_instance
                }
            }
            sla_test_map['sla_tests'][test_name] = tst_config

            # DO Agent test
            test_name = "Digital Ocean Agent " + public_ip
            tst_config = {
                "test_name": test_name,
                "is_enabled": False,
                "required_parameters": [],
                "outputs": ["storage_influx_1", "result_file"],
                "suite": "Digital Ocean Agent",
                "action": {
                    "description": "Test " + public_ip,
                    "action_type": "ssh_remote_digital_ocean_agent",
                    "ssh_host": public_ip,
                    "ssh_rsa_private_key": "{{ ssh_key_path }}",
                    "ssh_user": "{{ ssh_user }}",
                    "sla_meta": vm_instance
                }
            }
            sla_test_map['sla_tests'][test_name] = tst_config

            # Uptime test
            test_name = "Uptime " + public_ip
            tst_config = {
                "test_name": test_name,
                "is_enabled": False,
                "required_parameters": [],
                "outputs": ["storage_influx_1", "result_file"],
                "suite": "Server Uptime",
                "action": {
                    "description": "Test " + public_ip,
                    "action_type": "ssh_uptime_more_then_x_days",
                    "ssh_host": public_ip,
                    "ssh_rsa_private_key": "{{ ssh_key_path }}",
                    "ssh_user": "{{ ssh_user }}",
                    "sla_meta": vm_instance
                }
            }

            sla_test_map['sla_tests'][test_name] = tst_config

            # Log Rotation test
            test_name = "Log Rotation " + public_ip
            tst_config = {
                "test_name": test_name,
                "is_enabled": False,
                "required_parameters": [],
                "outputs": ["storage_influx_1", "result_file"],
                "suite": "Log Rotation",
                "action": {
                    "description": "Test " + public_ip,
                    "action_type": "ssh_log_rotation_enabled",
                    "ssh_host": public_ip,
                    "ssh_rsa_private_key": "{{ ssh_key_path }}",
                    "ssh_user": "{{ ssh_user }}",
                    "sla_meta": vm_instance
                }
            }

            sla_test_map['sla_tests'][test_name] = tst_config

            # #
            # # Varnish Checkpoints
            # #
            # test_name = "Varnish Failed Backend Connections"
            # tst_config = {
            #     "test_name": test_name,
            #     "is_enabled": True,
            #     "required_parameters": [],
            #     "outputs": ["storage_influx_1", "result_file"],
            #     "suite": "Log Rotation",
            #     "action": {
            #         "description": "Varnish Failed Backend Connections",
            #         "action_type": "ssh_log_rotation_enabled",
            #         "ssh_host": public_ip,
            #         "ssh_rsa_private_key": "{{ ssh_key_path }}",
            #         "ssh_user": "{{ ssh_user }}",
            #         "sla_meta": vm_instance
            #     }
            # }

            sla_test_map['sla_tests'][test_name] = tst_config

        return sla_test_map