import logging
from gunicorn.app.base import Application, Config
import gunicorn
from gunicorn import glogging
from gunicorn.workers import sync
from flask import Flask, request, abort, has_request_context
from flask_jsonpify import jsonpify
import multiprocessing
import urllib3
import sys
import os
import grp
import pwd
from subprocess import Popen, PIPE, STDOUT
import requests
import json
import time
# from sempahore import OperationsSemaphore
from flask_request_id_header.middleware import RequestID
import uuid
import threading
from flask import Flask
import sys
import tempfile
import os
import json
import subprocess
from flask_cors import CORS



app = Flask(__name__, instance_relative_config=True)
cors = CORS(app, resources={r"/*": {"origins": "*"}})

@app.route('/ping', methods=['GET'])
def get_grafana_notification():
    return jsonpify("pong")

@app.route('/compile-python-syntax', methods=['POST'])
def compile_python_syntax():
    json = request.json
    try:
        codeObejct = compile(json["syntax"], "randomfile", "exec")
        print(str(codeObejct))
        return jsonpify({"status": "valid"})
    except Exception as e:
        return jsonpify({"status": "invalid"})


@app.route('/run_test', methods=['POST'])
def run_test():
    json_dict = request.json
    try:
        file_path = "./stress_test_scenario.json"
        file = open(file_path, "w")
        file.write(json.dumps(json_dict['test'], indent=4))
        file.close()

        print("Current working directory is: " + os.getcwd())
        print("generating stress test python code")
        p = subprocess.Popen('env/bin/python generator.py stress_test_scenario.json', shell=True, stdout=subprocess.PIPE)
        out, err = p.communicate()
        p.wait()

        if p.returncode != 0:
            return jsonpify({
                "status": "error",
                "message": "Can not compile test, error status code returned",
                "err": str(err),
                "out": str(out)
            })

        p_docker = subprocess.Popen('./bin/restart_docker.sh ./compiled_docker_compose.yml 4', shell=True, stdout=subprocess.PIPE)
        out_docker, err_docker = p_docker.communicate()
        p_docker.wait()

        if p_docker.returncode != 0:
            return jsonpify({
                "status": "error",
                "message": "Error while restarting docker, return code is " + str(p_docker.returncode) + ": " + str(err_docker)
            })

        return jsonpify({
            "status": "ok",
            "err_compile": str(err),
            "out_compile": str(out),
            "err_docker": str(err_docker),
            "out_docker": str(out_docker)
        })

    except Exception as e:
        return jsonpify({"status": "error", "exception": str(e)})

app.run("0.0.0.0", 8088, True, False)
