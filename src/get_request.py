class GetRequest:

    @staticmethod
    def get(client, url, verify, timeout, allow_redirects):
        return client.get(url, verify=verify, timeout=timeout, allow_redirects=allow_redirects)

