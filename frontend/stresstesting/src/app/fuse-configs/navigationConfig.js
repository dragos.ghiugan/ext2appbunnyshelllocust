import { authRoles } from 'app/auth';
import i18next from 'i18next';
import DocumentationNavigation from '../main/documentation/DocumentationNavigation';
import ar from './navigation-i18n/ar';
import en from './navigation-i18n/en';
import tr from './navigation-i18n/tr';

i18next.addResourceBundle('en', 'navigation', en);
i18next.addResourceBundle('tr', 'navigation', tr);
i18next.addResourceBundle('ar', 'navigation', ar);

const navigationConfig = [
	{
		id: 'stress-testing',
		title: 'Stress Testing',
		translate: 'STRESS TESTING',
		type: 'group',
		icon: 'apps',
		children: [
			{
				id: 'stress-testing-config',
				title: 'Json Builder',
				type: 'item',
				url: '/stress/testing/jsonbuilder'
			}
		]
	}
];

export default navigationConfig;
