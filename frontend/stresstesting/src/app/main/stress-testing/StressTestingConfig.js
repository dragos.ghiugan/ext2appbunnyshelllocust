import React from 'react';
import { Redirect } from 'react-router-dom';

const StressTestingConfig = {
	settings: {
		layout: {
			footer: {
				display: false
			}
		}
	},
	routes: [
		{
			path: '/stress/testing/jsonbuilder',
			component: React.lazy(() => import('./StressTesting'))
		}
	]
};

export default StressTestingConfig;
