import json
import sys
import os
import logging
from jinja2 import Template
from src.variables_crapper import extract_variables, identify_external_variables, extract_variables_from_html, identify_external_variables
from lxml.html import fromstring


def test_selectors():
    with open('./tests/html/innobyte.com.html','r') as file:
        innobyte_html = file.read()
        document = fromstring(innobyte_html)
        variables_extracted = {}
        extract_variables_from_html(
            {
                "h1": "first_value  => css: #header-partners =>  attribute: class",
                "h2": "count_nodes  => css: #header-partners =>  attribute: class",
                "h3": "nodes        => css: #header-partners =>  attribute: class",
                "h4": "first_value  => css: #header-partners => attribute: class",
                "h5": "count_nodes  => css: #header-partners",
                "h6": "nodes        => css: #header-partners",
                "h7": "all_values   => css: #header-partners => attribute: class"
            },
            document,
            innobyte_html,
            variables_extracted
        )
        print(variables_extracted)

def test_variables_eval():
    query = "/categorie/masti/?min_price={{ eval: random.randint(1, 33)  }}&max_price=70"
    print(identify_external_variables(query, {}))


# test_selectors()
test_variables_eval()