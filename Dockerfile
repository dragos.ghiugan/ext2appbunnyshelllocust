FROM ubuntu:20.04

RUN apt-get update
RUN apt-get install -y software-properties-common
RUN add-apt-repository -y ppa:deadsnakes/ppa

RUN apt-get update \
  && DEBIAN_FRONTEND=noninteractive apt-get install -y \
    build-essential \
    libncursesw5-dev \
    libreadline-dev \
    libssl-dev \
    libgdbm-dev \
    libc6-dev \
    libsqlite3-dev \
    libxml2-dev \
    libxslt-dev \
    libevent-dev \
    locales \
    git \
    htop \
    telnet \
    python3.9-dev \
    python3.9 \
    python3.9-distutils \
    python-dev \
    python3-pip \
  && apt-get clean

RUN pip3 install --upgrade setuptools
RUN pip3 install --upgrade pip
RUN pip3 install --upgrade distlib

ENV BUNNYSHELL_LOCUST_APPLICATION_VERSION=1.0.0

COPY requirements.txt /requirements.txt

RUN python3 -m pip install -r /requirements.txt

WORKDIR /app

COPY docker_start.sh /docker_start.sh
RUN chmod 755 /docker_start.sh

EXPOSE 8089 5557 5558
WORKDIR /app

ENV LANG=en_US.UTF-8
ENV LC_ALL=en_US.UTF-8

ENV BUNNYSHELL_LOCUST_APPLICATION_VERSION=1.0.0

CMD ["./docker_start.sh"]
