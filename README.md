# Clone project

token user: bunnyshell
token password: 71ykNmAQawGyQozwxQx5

````
git clone https://bunnyshell:71ykNmAQawGyQozwxQx5@gitlab.com/bunnyshell/ext2appbunnyshelllocust.git
````

### Links
- grafana: http://127.0.0.1:3000/
- locust: http://127.0.0.1:8089/
- exporter: http://127.0.0.1:9646/metrics

# Install Deps on Linux / Mac os x

##Linux
````
export LC_ALL="en_US.UTF-8"
export LC_CTYPE="en_US.UTF-8"

sudo add-apt-repository ppa:deadsnakes/ppa
sudo apt update
apt-get install python3.9-dev python3.9-venv
pip install --upgrade pip

python3.9 -m venv env
env/bin/pip3.9 install --upgrade pip
env/bin/pip3.9 install -r requirements.txt
source env/bin/activate

````

### Mac OS X 

````
export LC_ALL="en_US.UTF-8"
export LC_CTYPE="en_US.UTF-8"
brew install byenv
pyenv install  3.6.13     
````

````
docker-compose up --scale slave=1
````

````
export FLASK_APP=api.py && export FLASK_ENV=development && flask run
````


#Generate test file

Create a file .json which describes the stress testing
Documentation here https://bunnyshell.atlassian.net/wiki/spaces/SER/pages/696975376/Stress+Testing+Map+-+docs
````
python generator.py <path to json file>
````

# Build containers
```
./bin/restart_docker.sh compiled_docker_compose.yml <number of containers>
```

# Build to binary
````
env/bin/pyinstaller sla_test.py --onefile
````
will create in dist folder the binary file with all dependencies

# SLA
### Export entire project path
````
export PYTHONPATH="$PWD"  
````

### Build definitions and grafana charts for bestvalue
````
python src/monitor/presentation/command/console.py app:monitoring:build  --sla-dir /opt/sla_specs/definition --build-dir /opt/sla_specs/build
````

### Execute command
````
python src/monitor/presentation/command/console.py app:monitoring:execute:definition  --definition /opt/sla_specs/build/build_bestvalue.yml --params /opt/sla_specs/definition/bestvalue/parameters.yml
````

w