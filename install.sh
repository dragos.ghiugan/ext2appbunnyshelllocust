# Commented set -ex because it's failing at installing python pip which is deprecated.
#set -ex

echo "Install docker and docker compose"
# install docker and docker compose
apt-get install  -y   apt-transport-https     ca-certificates     curl     gnupg-agent     software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
apt-key fingerprint 0EBFCD88
add-apt-repository    "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
apt-get update
apt-get install -y docker-ce docker-ce-cli containerd.io
curl -L "https://github.com/docker/compose/releases/download/1.27.3/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

if [ -f "/usr/bin/docker-compose" ]; then
  rm /usr/bin/docker-compose
fi

ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose

# virtualenv
apt-get install -y virtualenv

# python packages
apt-get install -y python3-pip
apt-get install -y python-pip
apt-get install git


function kill_remove_docker_containers() {
    echo "Killing/stopping containers..."
    nr_containers=$(docker ps -q | wc -l | sed -e 's/^[[:space:]]*//')
    echo "Number of containers found: $nr_containers"

    if [[ "$nr_containers" -eq "0" ]]; then
       echo "No containers found to kill"
    else
        echo "Detected containers to kill"
        docker kill $(docker ps -q)
        docker rm $(docker ps -a -q)
    fi
}

kill_remove_docker_containers

echo "Install project..."

# clone project
virtualenv -p python3.9 venv
venv/bin/pip install -r requirements.txt

echo "Next in line:"
echo "  1. write your test json file"
echo "  2. generate test files from json (json as testing):"
echo  "     python generator.py <path to json>"
echo "  3. execute command which starts / restarts machines"
echo "      bash ./bin/restart_docker.sh `pwd`/compiled_docker_compose.yml `nproc --all`"
