import requests
import re
from urllib.parse import urlparse, urljoin
from bs4 import BeautifulSoup
# import colorama
import argparse
import sys
import json


# colorama.init()
# GREEN = colorama.Fore.GREEN
# GRAY = colorama.Fore.LIGHTBLACK_EX
# RESET = colorama.Fore.RESET


def is_valid(url):
    """
    Checks whether `url` is a valid URL.
    """
    parsed = urlparse(url)
    return bool(parsed.netloc) and bool(parsed.scheme)

def get_all_website_links(url):
    """
    Returns all URLs that is found on `url` in which it belongs to the same website
    """

    internal_urls = set()
    external_urls = set()

    # all URLs of `url`
    urls = set()
    # domain name of the URL without the protocol
    domain_name = urlparse(url).netloc
    response = requests.get(url, verify=False, timeout=30, stream=True)
    print(response.headers)
    # sock = response.raw._connection.sock.getnameinfo()
    # print(sock)
    # exit(0)
    # print("Server ip/port: " + ip + "/" + str(port))
    soup = BeautifulSoup(response.content, "html.parser")

    print("Domain name is ", domain_name)

    for a_tag in soup.findAll("a"):
        href = a_tag.attrs.get("href")
        if href == "" or href is None:
            # href empty tag
            continue
        href = urljoin(url, href)

        parsed_href = urlparse(href)
        href = parsed_href.scheme + "://" + parsed_href.netloc + parsed_href.path
        if not is_valid(href):
            # not a valid URL
            continue
        if href in internal_urls:
            # already in the set
            continue

        if "void(0)" in href:
            continue

        if "javascript:" in href:
            continue

        if "mailto://" in href:
            continue
        if "/tel:" in href:
            continue

        #non www
        # if "www" in href:
        #     continue

        # accept only urls with strict domain name
        if href.startswith("http://") or href.startswith("https://"):
            href_domain = parsed_href.netloc
            print("Href domain", href_domain, href)
            if href_domain.startswith("www."):
                if domain_name.startswith("www."):
                    if href_domain != domain_name:
                        print("Skip, different domains", href_domain, domain_name, href)
                        continue
                elif href_domain != ("www." + domain_name):
                    print("Skip, different domains", href_domain, ("www." + domain_name), href)
                    continue
            elif domain_name.startswith("www."):
                if domain_name != ("www." + href_domain):
                    print("Skip, different domains", ("www." + href_domain), domain_name, href)
                    continue
            elif domain_name != href_domain:
                print("Skip, different domains", href_domain, domain_name, href)
                continue

        if domain_name not in href and not href.startswith("/"):
            # external link
            if href not in external_urls:
                # print(f"[!] External link: {href}")
                external_urls.add(href)
            continue

        # print(f"[*] Internal link: {href}")
        urls.add(href)
        internal_urls.add(href)
    return urls

def crawl_total_urls(url, max_links=100, limit_requests = 10):
    """
    Crawls a web page and extracts all links.
    You'll find all links in `external_urls` and `internal_urls` global set variables.
    params:
        max_urls (int): number of max urls to crawl, default is 30.
    """
    total_requests_fetched = 0
    urls_to_fetch = [url]
    internal_links = []
    fetched_link = []

    while len(internal_links) <= max_links and limit_requests >= total_requests_fetched and len(urls_to_fetch) > 0 :
        urls_to_fetch = list(dict.fromkeys(urls_to_fetch))
        # print(str(len(urls_to_fetch)))

        total_requests_fetched += 1
        if len(urls_to_fetch) == 0:
            print("No more urls to fetch")
            break

        url_to_fetch = urls_to_fetch.pop()
        if url_to_fetch in fetched_link:
            print("Link already fetched")
            continue

        links = get_all_website_links(url_to_fetch)
        fetched_link.append(url_to_fetch)

        for link in links:
            if len(internal_links) >= max_links:
                break
            if link not in fetched_link:
                urls_to_fetch.append(link)
                internal_links.append(link)

    return internal_links

def main():
    """
    Usage:
        python crawl.py https;//google.com 50 50
    :return:
    """
    website = sys.argv[1]
    number_of_links = sys.argv[2]
    number_of_requests = sys.argv[3]

    internal_links_all = crawl_total_urls(str(website), int(number_of_links), int(number_of_requests))
    print("[+] Total Internal links:", internal_links_all)
    print("[+] Count internl links:", len(internal_links_all))

    removed_duplicats = list(dict.fromkeys(internal_links_all))
    print(str(len(removed_duplicats)))

    print("[+] Total Internal links:", removed_duplicats)
    print("[+] Count internl links:", len(removed_duplicats))

    # json_formatted_str = json.dumps(json_object, indent=2)
    new_string = re.sub("[^0-9a-zA-Z]+", "_", website)
    with open('tmp/crawler_' + new_string, 'w') as outfile:
        json.dump(internal_links_all, outfile, indent=4)


if __name__ == "__main__":
    main()



